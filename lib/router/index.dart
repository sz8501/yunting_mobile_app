import 'package:flutter/material.dart';
import '../views/index.dart';
import '../views/test.dart';
import '../views/login.dart';

class YtRouter {
  static final Map<String, WidgetBuilder> routes = {
    '/': (BuildContext context) => const IndexPage(),
    '/test': (BuildContext context) => const TestPage(),
    '/login': (BuildContext context) => const LoginPage()
  };
}
