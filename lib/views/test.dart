import 'package:flutter/material.dart';

class TestPage extends StatefulWidget {
  const TestPage({super.key});
  @override
  State<StatefulWidget> createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: const Text('测试页面'),
    );
  }
}
